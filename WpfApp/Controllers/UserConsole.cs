﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace WpfApp
{
    public class UserConsole
    {
        private FileStream FS;
        private string logPath;
        public List<DebugLog> logs { get; private set; }
        public Object o;
        public DateTime Today;
        public UserConsole(){
            this.Today = new DateTime();
            this.Today = DateTime.Now;
            this.logPath = @"C:\Users\Roz\Documents\Logs_"+ this.Today.ToString("yyyy_MM_dd")+ ".json";
            this.CreateLogFile();
            this.readLogs();

        }
        private void readLogs() {
            this.FS = new FileStream(this.logPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            StreamReader sr = new StreamReader(this.FS);
            string str = sr.ReadToEnd();
            this.FS.Close();
            Console.WriteLine(str);
            this.logs = JsonConvert.DeserializeObject<List<DebugLog>>(str);
            try {
                Console.WriteLine("LOG start ! ");
                Console.WriteLine(this.logs[0].Content);
                Console.WriteLine("LOGU KONIEC ! ");
            } catch (NullReferenceException nre) {
                this.logs = new List<DebugLog>();
            }
            

            
        }
        public void AddConsoleLog(string con,string type,DateTime dt) {
            DebugLog newLog = new DebugLog(con, type,dt);
            this.logs.Add(newLog);
            char[] c = con.ToCharArray();
            
        }
        public void saveJson() {
            this.FS = new FileStream(this.logPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
            this.FS.Close();
            StreamWriter sw = new StreamWriter(this.logPath, false, Encoding.ASCII);
            string str = JsonConvert.SerializeObject(this.logs, Formatting.Indented);
            sw.Write(str);
            sw.Close();
        }
        private void CreateLogFile() {
           
            this.FS = new FileStream(this.logPath, FileMode.OpenOrCreate);
            this.FS.Close();
        }
    }

    public class DebugLog
    {
        public DebugLog(string con, string type, DateTime dt = new DateTime()) {
            this.Content = con;
            this.Type = type;
            this.OutTime = dt;

        }
        public bool isEmpty() {
            if (this.Content.Length == 0) {
                return true;
            }
            else {
                return false;
            }
        }
        public DateTime OutTime;
        public String _OutTime { get { return OutTime.ToString("yyyy-MM-dd HH:mm:ss"); } set { OutTime = DateTime.Parse(value); } }
        public string Type;

        public string Content;
    }
}
