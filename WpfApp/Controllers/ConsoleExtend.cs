﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO.Log;
using System.IO;

namespace WpfApp
{
    public class SystemConsole
    {

        [DllImport("Kernel32.dll")]
        public static extern bool AttachConsole(IntPtr processId);
        [DllImport("kernel32.dll", EntryPoint = "AllocConsole", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int AllocConsole();
        public Stream ErrorStream;
        public Stream InputStream;
        public Stream OutputStream;
        public TextReader InputReader;
        public TextWriter OutputWriter;
        public string Input = "";
        public byte[] InputBuffer;
        public char[] InputReaderBuffer;
        public string Output = "";
        public byte[] OutputBuffer;
        public char[] OutputWriterBuffer;
        public Thread InThr;
        public int c = 0;
        public Task<int> Tasks;
        public List<Action<string>> OnInput;

        public SystemConsole() {
            this.OnInput = new List<Action<string>>();
            this.ErrorStream = Console.OpenStandardError();
            this.ErrorStream.Dispose();
            this.InputStream = Console.OpenStandardInput();
            this.OutputStream = Console.OpenStandardOutput();
            this.InputBuffer = new byte[1024];
            this.InputReaderBuffer = new char[1024];
            this.OutputWriterBuffer = new char[1024];
            this.OutputBuffer = new byte[1024];
          
            IntPtr hWnd = System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle;
            SystemConsole.AttachConsole(hWnd);
            this.InputReader = TextReader.Synchronized(TextReader.Null);
            this.OutputWriter = TextWriter.Synchronized(TextWriter.Null);
            // Console.SetIn(this.InputReader);
            // Console.SetOut(this.OutputWriter);
            this.InThr = new Thread(this.Init);
            Task t = new Task(this.test);
            t.Start();
            //this.InThr.Start();
        }

        public async void Init(Object obj)
        {
            this.InitIR();
            this.InitOR();
            //   this.InitITR();
            //   this.InitOTR();
            Console.WriteLine(Console.Out.ToString());
            this.Runer();
        }

        public static void UiInvoke(Action<string> a)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(a);
        }

        private async void test()
        {
            byte[] a = new byte[10];
            Stream s = Console.OpenStandardInput();
            string z = "";
            string zz = "";
            int ad = 0;
            int x = 0;
            Thread.AllocateDataSlot();
            while (true)
            {
                
                zz = await Console.In.ReadLineAsync();
                this.Input += zz;
                Object asss = new Object[2];
                Action<string>[] copy = new Action<string>[this.OnInput.Count];
                this.OnInput.TrimExcess();
                this.OnInput.CopyTo(copy);
                foreach (Action<string> onTrg in copy) {
                    await Task.Run(() => onTrg(this.Input) );
                }
                //  this.PPP.Dispatcher.Invoke(this.updateAction, "dddd");


               

                //DebugWindow.ins.Dispatcher.Invoke(new Action(() => DebugWindow.Inv(this.Input)), System.Windows.Threading.DispatcherPriority.Normal);
                App.Current.Dispatcher.Invoke(App.Callback(zz,DebugWindow.Inv), System.Windows.Threading.DispatcherPriority.Normal);
                Task.Delay(100);
                // Console.WriteLine(z + " /-----------------------------------TASK KURWO ! -----------------------------------/");
            }
            
            // s.Dispose();


        }

        public async void Runer() {

                this.InThr = new Thread(this.Init);
                this.c++;
           
                Console.WriteLine(this.c.ToString() + " ============================================== New runner ==============================================");
                this.InThr.Start();
            
        }
        public async void InitITR()
        {
            await this.InputReader.ReadAsync(this.InputReaderBuffer, 0, 1024);
            
        }
        public void AddOnInputListener(Action<string> a) {
            this.OnInput.Add(a);
        }
        public async void InitOTR()
        {
             await this.OutputWriter.WriteAsync(this.InputReaderBuffer, 0, 1024);
        }

        public async void InitIR() {
            await this.InputStream.ReadAsync(this.InputBuffer, 0, 1024);
            this.Input += string.Concat(Encoding.ASCII.GetString(this.InputBuffer));
        }

        public async void InitOR()
        {
            await this.OutputStream.WriteAsync(this.OutputBuffer, 0, 1024);
            this.Output+=string.Concat(Encoding.ASCII.GetString(this.OutputBuffer));
            Console.WriteLine("ALEEELUJA ==========================================");
        }
    }

}
