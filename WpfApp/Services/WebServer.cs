﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Diagnostics.SymbolStore;
using System.Net.Sockets;
using Newtonsoft.Json;



namespace WpfApp.Services
{
    
    public partial class WebServer{
        private Thread MainThread;
        private Socket ServerSocket;
        public ConnectedClientsServeThread ServeThread;
        private IAsyncResult AcceptResult;
//        public Task<TcpClient> SS;
 //       public TcpListener sp;
//        public List<NetworkStream> NetworkStreams; 
//        public List<TcpClient> TcpClients;
        public List<ConnectedClientSocket> Clients;
 //       public SocketAsyncEventArgs SAEA;
        public bool isRuned = false;
        public WebServer() {
            this.MainThread = new Thread(this.onStart);
 //           this.SAEA = new SocketAsyncEventArgs();
            this.ServeThread = new ConnectedClientsServeThread();
            this.isRuned = true;


            // this.SS = new Task<TcpClient>();
            //System.Net.EndPoint();
        }
        public static void Res()
        {
            App.Scope.WS = null;
            App.Scope.WS = new WebServer();
        }
        public void RestartServer()
        {
            this.ServerSocket.EndAccept(this.AcceptResult);
            this.ServerSocket.Poll(0, SelectMode.SelectError);
            this.ServerSocket.Dispose();
            this.ServerSocket.Shutdown(SocketShutdown.Both);
            this.ServerSocket.Close();
            foreach (var cli in this.Clients)
            {
                cli.CliSock.EndAccept(this.AcceptResult);
                cli.CliSock.Disconnect(false);
                cli.CliSock.Dispose();
                cli.CliSock.Poll(0, SelectMode.SelectError);
                cli.CliSock.Shutdown(SocketShutdown.Both);
                cli.CliSock.Close();

            }





            /* this.MainThread.Abort();
             this.ServeThread.ServClientsThread.Abort();
             GC.Collect(GC.GetGeneration(this.ServerSocket));
             GC.Collect(GC.GetGeneration(this.ServeThread));
             GC.Collect(GC.GetGeneration(this));
            */
            WebServer.Res();

            //this.ServeThread = new ConnectedClientsServeThread();
            // this.MainThread = new Thread(this.onStart);
            //this.MainThread.Start();
        }
        public void StartServer()
        {
            if (this.MainThread.ThreadState == ThreadState.Unstarted)
                this.MainThread.Start();
            else
            {
             //   this.RestartServer();
            }

        }
        public void onStart(Object obj){
            
            this.ServeThread.StartThread(ref this.Clients);
            
            this.ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            Console.WriteLine("ServerSocket Started !");
            this.ServerSocket.Bind(new IPEndPoint(IPAddress.Any, 8080));
            this.ServerSocket.Listen(128);

            this.AcceptResult = this.ServerSocket.BeginAccept(null, 0, this.OnAccept, null);

        }

        public async void OnAccept(IAsyncResult result)
        {
            string headerResponse = "";

            byte[] buffer = new byte[1024];
            int CurID = this.Clients.Count;
            
            try
            {
                this.Clients.Add(new ConnectedClientSocket());
                if (this.ServerSocket != null && this.ServerSocket.IsBound){

                    this.Clients[CurID].Sock = this.ServerSocket.EndAccept(result);
                 //   client = client.Accept();

                    var i = this.Clients[CurID].Sock.Receive(buffer);
                    headerResponse = Encoding.UTF8.GetString(buffer);
                    Console.WriteLine("Recived");// write received data to the console
                    Console.WriteLine(Encoding.ASCII.GetString(buffer));
                }

                if (this.Clients[CurID].Sock != null){
                    /* Handshaking and managing ClientSocket */
                    Console.WriteLine("Handshake started !");
                    List<string> listOfHeaders = new List<string>();
                    listOfHeaders = headerResponse.Split('\n').ToList();
                    WebClient wc = new WebClient();
                   
                    foreach (var header in listOfHeaders)
                    {
                        try
                        {
                            wc.Headers.Add(header);
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    String kk = wc.Headers.Get("Sec-WebSocket-Key");
                    Console.WriteLine("Web-socket-key DEBUG");
                    Console.WriteLine("                 "+kk);
                    Console.WriteLine("/Web-socket-key DEBUG");
                    //var writer = System.Xml. () XmlWriter.Create();
                   // writer.Write(objectToSerialize);
                   // Console.WriteLine(JsonConvert.SerializeObject(this.Clients, Formatting.Indented));
                    string res = Utilities.AcceptRes(kk);
                    this.Clients[CurID].Sock.Send(Encoding.UTF8.GetBytes(res) );
                         buffer = new byte[1024];
                    var i = this.Clients[CurID].Sock.Receive(buffer);
                    //this.ServerSocket.Connect(client.RemoteEndPoint);
                    Console.WriteLine(i.ToString());
                    Console.WriteLine(Utilities.GetDecodedData(buffer,i));
                    Task.Run(this.Clients[CurID].KeepCon);
                    
                }
                
            }catch (SocketException exception){
                Console.WriteLine(exception.Message.ToString());
            }finally {
                if (this.ServerSocket != null && this.ServerSocket.IsBound)
                {
                   this.AcceptResult= this.ServerSocket.BeginAccept(null, 0, this.OnAccept, null);
                    //Task.Run((new Action(() => this.ServerSocket.BeginAccept(null,0, this.OnAccept, null))));
                }
            }
        }



    }
    

  
}
