﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Security.Cryptography;
using System.Net.Sockets;


namespace WpfApp.Services
{


        public delegate WebServer.ConnectedClientSocket GetConnectedClientSocket(int Index);
        public delegate bool SendMsgToConnectedClient(int Index, string Msg);

    public partial class WebServer
    {
        public class ConnectedClientSocket
        {
            public Socket CliSock;
            public Action ReciveAsync;
            public Action SendAsync;
            public Action KeepCon;
            public Socket Sock { get { return this.CliSock; } set { this.CliSock = value; } }
            public SocketAsyncEventArgs RecivedAsyncEventArgs;
            public SocketAsyncEventArgs SendedAsyncEventArgs;
            public byte[] ReciveBuf;
            public byte[] SendBuf;
            public string Recived;
            public string Sended;
            public bool KeepConection;

            public ConnectedClientSocket()
            {
                this.KeepCon = new Action(() => this.KeepConected());
                this.CliSock = null;
                this.KeepConection = true;
                this.Recived = "";
                this.Sended = "";
                this.ReciveBuf = new byte[1024];
                this.SendBuf = new byte[1024];
                this.RecivedAsyncEventArgs = new SocketAsyncEventArgs();
                this.SendedAsyncEventArgs = new SocketAsyncEventArgs();
                this.ReciveAsync = new Action(() => this.CliSock.ReceiveAsync(this.RecivedAsyncEventArgs));
                this.SendAsync = new Action(() => this.CliSock.SendAsync(this.SendedAsyncEventArgs));
            }
            public void BufReset()
            {
                this.Recived = "";
                this.Sended = "";
                this.ReciveBuf = new byte[1024];
                this.SendBuf = new byte[1024];
            }

            public void FinishKeepConected()
            {
                this.KeepConection = false;
            }
            public async void KeepConected()
            {
                while (this.KeepConection)
                {

                    while (this.Sock.Available > 0)
                    {

                        //ConSock.Receive(InBuf);
                        // this.BufReset();
                        this.Sock.Receive(this.ReciveBuf);

                        this.Recived += ASCIIEncoding.ASCII.GetString(this.ReciveBuf);
                        string deb = Utilities.GetDecodedData(this.ReciveBuf, this.ReciveBuf.Length);
                        Console.WriteLine("____________________SOCKET_MSG________________________");
                        Console.WriteLine(deb);
                        Console.WriteLine("____________________SOCKET_MSG________________________");
                        this.SendMsg(deb);

                    }
                    App.consol.AddOnInputListener(new Action<string>((x) => this.CliSock.Send(Utilities.EncodeMessageToSend(x.Substring(0, x.Length)))));
                    // Task.Run(SendAsync);
                    // OutBuf = System.Text.Encoding.UTF8.GetBytes("Ping");

                    //ConSock.SendPacketsAsync(SAEA);

                    Thread.Sleep(50);

                }

            }

            public async void SendMsg(string msg)
            {
                this.SendBuf = Utilities.EncodeMessageToSend(msg);
                this.SendedAsyncEventArgs = new SocketAsyncEventArgs();
                this.SendedAsyncEventArgs.SetBuffer(this.SendBuf, 0, this.SendBuf.Length);
                await Task.Run(this.SendAsync);
            }

        }
    }
}
