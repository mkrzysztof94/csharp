﻿using System;
using System.Web;
namespace WpfApp.Services
{   
    partial class WSocket
    {
        /// <summary> 
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod wygenerowany przez Projektanta składników

        /// <summary> 
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.serviceController1 = new System.ServiceProcess.ServiceController();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.process1 = new System.Diagnostics.Process();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            // 
            // process1
            // 
            this.process1.StartInfo.Domain = "localhost";
            this.process1.StartInfo.LoadUserProfile = false;
            this.process1.StartInfo.Password = null;
            this.process1.StartInfo.StandardErrorEncoding = null;
            this.process1.StartInfo.StandardOutputEncoding = null;
            this.process1.StartInfo.UserName = "Roz";
            this.process1.Exited += new System.EventHandler(this.process1_Exited);
            // 
            // Socket
            // 
            this.ServiceName = "HttpSocket";
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();

        }
        public void StartHttpAt() {
            this.HttpListener.Start();
            this.HttpListener.Prefixes.Add("http://127.0.0.1/a/");
            this.HttpListener.GetContext();
            Object o = new Object();
            System.AsyncCallback x = new System.AsyncCallback(this.callAss);
            this.HttpListener.BeginGetContext(x, o);
        }
        #endregion
         void callAss(System.IAsyncResult r) {
            Console.WriteLine("TO TUTAJ !");

            Console.WriteLine(r);

            Console.WriteLine("TO TUTAJ !");
            //App.Scope.

        }
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ServiceProcess.ServiceController serviceController1;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Diagnostics.Process process1;
        private System.Net.HttpListener HttpListener;
    } 
}
