﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace WpfApp.Services
{
    public partial class WebServer
    {
        public  static class Utilities
        {
            public static string guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

            public static string AcceptRes(string key)
            {
                return "HTTP/1.1 101 Switching Protocols\r\n" + "Connection: Upgrade\r\n" + "Upgrade: websocket\r\n" + "WebSocket-Protocol: 13\r\n" + "Sec-WebSocket-Accept: " + Utilities.AcceptKey(ref key) + "\r\n\r\n";
            }

            public static string GetDecodedData(byte[] buffer, int length)
            {
                byte b = buffer[1];
                int dataLength = 0;
                int totalLength = 0;
                int keyIndex = 0;

                if (b - 128 <= 125)
                {
                    dataLength = b - 128;
                    keyIndex = 2;
                    totalLength = dataLength + 6;
                }

                if (b - 128 == 126)
                {
                    dataLength = BitConverter.ToInt16(new byte[] { buffer[3], buffer[2] }, 0);
                    keyIndex = 4;
                    totalLength = dataLength + 8;
                }

                if (b - 128 == 127)
                {
                    dataLength = (int)BitConverter.ToInt64(new byte[] { buffer[9], buffer[8], buffer[7], buffer[6], buffer[5], buffer[4], buffer[3], buffer[2] }, 0);
                    keyIndex = 10;
                    totalLength = dataLength + 14;
                }

                if (totalLength > length)
                    throw new Exception("The buffer length is small than the data length");

                byte[] key = new byte[] { buffer[keyIndex], buffer[keyIndex + 1], buffer[keyIndex + 2], buffer[keyIndex + 3] };

                int dataIndex = keyIndex + 4;
                int count = 0;
                for (int i = dataIndex; i < totalLength; i++)
                {
                    buffer[i] = (byte)(buffer[i] ^ key[count % 4]);
                    count++;
                }

                return Encoding.ASCII.GetString(buffer, dataIndex, dataLength);
            }

            public static string AcceptKey(ref string key)
            {
                string longKey = key + Utilities.guid;
                System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1CryptoServiceProvider.Create();
                byte[] hashBytes = sha1.ComputeHash(Encoding.ASCII.GetBytes(longKey));
                return Convert.ToBase64String(hashBytes);
            }

            public static byte[] EncodeMessageToSend(string message)
            {
                byte[] response;
                byte[] bytesRaw = Encoding.UTF8.GetBytes(message);
                byte[] frame = new byte[10];

                Int32 indexStartRawData = -1;
                Int32 length = bytesRaw.Length;

                frame[0] = (byte)129;
                if (length <= 125)
                {
                    frame[1] = (byte)length;
                    indexStartRawData = 2;
                }
                else if (length >= 126 && length <= 65535)
                {
                    frame[1] = (byte)126;
                    frame[2] = (byte)((length >> 8) & 255);
                    frame[3] = (byte)(length & 255);
                    indexStartRawData = 4;
                }
                else
                {
                    frame[1] = (byte)127;
                    frame[2] = (byte)((length >> 56) & 255);
                    frame[3] = (byte)((length >> 48) & 255);
                    frame[4] = (byte)((length >> 40) & 255);
                    frame[5] = (byte)((length >> 32) & 255);
                    frame[6] = (byte)((length >> 24) & 255);
                    frame[7] = (byte)((length >> 16) & 255);
                    frame[8] = (byte)((length >> 8) & 255);
                    frame[9] = (byte)(length & 255);

                    indexStartRawData = 10;
                }

                response = new byte[indexStartRawData + length];

                Int32 i, reponseIdx = 0;

                //Add the frame bytes to the reponse
                for (i = 0; i < indexStartRawData; i++)
                {
                    response[reponseIdx] = frame[i];
                    reponseIdx++;
                }

                //Add the data bytes to the response
                for (i = 0; i < length; i++)
                {
                    response[reponseIdx] = bytesRaw[i];
                    reponseIdx++;
                }

                return response;
            }
        } // --------------------------------------------------Koniec Utilities----------------------------//
       
    }

    /* WYJEBANE Z WEBSERV 
             public async Task<Task> ListenForTcp() {
            int cur = this.TcpClients.Count();

             TcpClients.Add(await this.sp.AcceptTcpClientAsync());
            // Socket ss=await this.sp.AcceptSocketAsync();
            //ss.Connect(ss.RemoteEndPoint);
            //Char[] send = string.Concat("HUJEW").ToCharArray();

            //ss.Send(Encoding.Unicode.GetBytes(send), send.Length, SocketFlags.None);
            //WebSok.
            Console.WriteLine("New connection");
            // wss.Connect(wss.RemoteEndPoint);

           
            //this.NetworkStreams[cur].
            this.NetworkStreams.Add(this.TcpClients[cur].GetStream());

             String shr = "HTTP/1.1 200 OK\nOrigin:\"http://127.0.0.1\"\nContent-Type: \"application/xhtml\"\nAccess-Control-Allow-Origin:\"*\"\nConnection: \"keep-alive\"\nPragma: \"no-cache\"\n";
            //String shr = "";
            Byte[] b = ASCIIEncoding.ASCII.GetBytes(shr.ToCharArray());
            this.NetworkStreams[cur].Write(b, 0, b.Length); 
            this.NetworkStreams[cur].Flush();

            //wss.Send(b, 0, SocketFlags.None);
            Console.WriteLine("DEBUG");
            Console.WriteLine("===========");
           //  return new Task(new Action(()=>this.readClientStream(cur)));
        
            
            //System.Net.HttpListenerContext Hl = await res.GetContextAsync();
           // System.Net.WebSockets.HttpListenerWebSocketContext hlwsc =await Hl.AcceptWebSocketAsync("ws");
           // System.Net.AuthenticationManager.Register(System.Net.AuthenticationManager.RegisteredModules.)

            
            Task a= new Task((new Action(() => this.readClientStream(cur))));
            a.Start();
            Task newClient = new Task((new Action(() => this.ListenForTcp())));
            newClient.Start();
            return newClient;
            // AcceptAsyncResult
            //Console.WriteLine(o.AsyncWaitHandle.Handle.ToString());
            // this.handleNewConnection(o);
        }
        public async void readClientStream(int streamID) {
            
            //System.Net.Http.HttpResponseMessage hrm = new System.Net.Http.HttpResponseMessage();
            //hrm.Headers.Connection.Add("Keep alive");
            //this.TcpClients[streamID].
           // hrm.Headers.Upgrade.Add();
            // System.Net.AuthenticationManager.R
            // this.TcpClients[streamID].Connect("localhost",8086);

            //opts.
            while (true)
            {
                int pointer = 0;
    String s = "";
    int z = this.NetworkStreams[streamID].ReadByte();
                while (this.NetworkStreams[streamID].DataAvailable)
                {
        s += (char)z;

        z = this.NetworkStreams[streamID].ReadByte();
    }

                if (s.Length > 0) {
        Console.WriteLine(s);
        Byte[] a = Encoding.Unicode.GetBytes(s.ToCharArray());
        this.NetworkStreams[streamID].Write(a, 0, a.Length);
        this.NetworkStreams[streamID].Flush();
    }

    Thread.Sleep(100);
    }


}


    
        public async void handleNewConnection(IAsyncResult o) {
            int cur = this.TcpClients.Count();
            
            this.NetworkStreams.Add(this.TcpClients[cur].GetStream());

            String shr = "HTTP/1.1 200 OK";
            Char[] c = shr.ToCharArray();
            Byte[] b = ASCIIEncoding.ASCII.GetBytes(c);
            this.NetworkStreams[cur].Write(b, 0, b.Length);
            Console.WriteLine("DEBUG");
            Console.WriteLine("===========");
            Console.WriteLine(this.NetworkStreams[cur].ToString());
            Console.WriteLine(o.ToString());
            Console.WriteLine(o.AsyncState);

        }



     */


}
