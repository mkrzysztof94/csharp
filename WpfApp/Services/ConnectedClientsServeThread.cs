﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace WpfApp.Services
{
    public partial class WebServer
    {
        public class ConnectedClientsServeThread
        {

            public List<ConnectedClientSocket> ConnectedClients;
            public Thread ServClientsThread;
            event GetConnectedClientSocket GetConnectedClientSocketEvent;
            event SendMsgToConnectedClient SendMsgToConnectedClientEvent;

            public ConnectedClientsServeThread()
            {
                this.ServClientsThread = new Thread(() => this.onStart());
                this.SendMsgToConnectedClientEvent = this.SendMsgToConnectedClient;
                this.GetConnectedClientSocketEvent = this.GetConnectedClient;
                this.ConnectedClients = null;
            }

            public void StartThread(ref List<ConnectedClientSocket> ConectedCopy )
            {
                if(this.ServClientsThread.ThreadState == ThreadState.Unstarted)
                this.ServClientsThread.Start();
                else
                {
                    this.ServClientsThread.Abort();
                    this.ServClientsThread = new Thread(() => this.onStart());
                    this.ServClientsThread.Start();
                }
                ConectedCopy = this.ConnectedClients = new List<ConnectedClientSocket>();
            }

            private void onStart()
            {
                Console.WriteLine("ServClientsThread Started !");
            }

            private ConnectedClientSocket GetConnectedClient(int Index)
            {
                if (this.ConnectedClients.ElementAtOrDefault(Index) != null)
                    return this.ConnectedClients[Index];
                else
                    return null;
            }
            private bool SendMsgToConnectedClient(int Index, string Msg)
            {
                if (this.ConnectedClients.ElementAtOrDefault(Index) != null)
                {
                    this.ConnectedClients[Index].SendMsg(Msg);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
