﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.ServiceProcess;
using System.Collections;
using System.Threading;
using Newtonsoft.Json;

namespace WpfApp
{
    
    /// <summary>
    /// Logika interakcji dla klasy App.xaml
    /// </summary>
    
    public partial class App : Application
    {
        public static Action Callback(String request, Action<string> callback)
        {
            return (new Action(() => callback(request)));
        }

        public static void InvokeActionCallback(Action<String> act,String zz) {
            App.Current.Dispatcher.Invoke(App.Callback(zz, act), System.Windows.Threading.DispatcherPriority.Normal);
        }

        private Config.Main Cfg;
        public static Scope Scope;
        public static SystemConsole consol;

        public App() {
            App.consol = new SystemConsole();
            this.Cfg = new Config.Main();
            App.Scope = new Scope();
            
            Console.Out.WriteLine("------------------------------------------");
            Console.WriteLine(App.consol.ToString());
            Console.WriteLine("------------------------------------------");
            //Console.Write((String)Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented));

            // SystemConsole.AllocConsole();
        }
   

    }
    
    public class Scope {
        public DebugWindow DebugWin;
        public ICollection<KeyValuePair<string, ServiceBase>> Services;
        public ICollection<KeyValuePair<string, Thread>> Threads;
        public Services.WebServer WS;

        public Scope() {
            this.DebugWin = new DebugWindow();
            this.DebugWin.LoadLogs();
            this.Services = new Dictionary<string, ServiceBase>();
            this.Threads = new Dictionary<string, Thread>();
            this.WS = new Services.WebServer();
        }

        public void addService(string ServiceName,ServiceBase ServiceInstance) {
            this.Services.Add(new KeyValuePair<string, ServiceBase>(ServiceName,ServiceInstance));
        }

        public void addThread(string ThreadeName, Thread ThreadInstance)
        {
            this.Threads.Add(new KeyValuePair<string, Thread>(ThreadeName, ThreadInstance));
        }
    }
    public static class Util { 

        public static void LogObject(Object log)
        {
            Console.WriteLine("-----------------------------------DEBUG-----------------------------------");
            Console.WriteLine("");

            Console.Write((String)JsonConvert.SerializeObject(log, Formatting.Indented));

            Console.WriteLine("");

            Console.WriteLine("-----------------------------------/DEBUG-----------------------------------");

        }

        public static void LogString(String log)
        {
            Console.WriteLine("-----------------------------------DEBUG-----------------------------------");
            Console.WriteLine("");

            Console.Write(log);

            Console.WriteLine("");

            Console.WriteLine("-----------------------------------/DEBUG-----------------------------------");

        }
    }


}
