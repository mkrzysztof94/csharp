﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp.View
{
    /// <summary>
    /// Logika interakcji dla klasy Log.xaml
    /// </summary>
    public partial class Log : UserControl
    {
        public Grid GridCotainer;

        dataBinds db;
        public Log(String Content, String Type,DateTime dt)
        {
            InitializeComponent();
            this.db = new dataBinds(dt, Type, Content);
            this.DataContext = this.db;
            this.Height = '*';
        }
       
        public RowDefinition getRowDefinition()
        {
            RowDefinition RD = new RowDefinition();
            RD.MinHeight = 20;
            return RD;
        }
        public class dataBinds
        {
            private DateTime DataValue;
            private string TypeValue;
            private string ConValue;
            public dataBinds(DateTime DataValue, string TypeValue, string ConValue)
            {
                this.DataValue = DataValue;
                this.TypeValue = TypeValue;
                this.ConValue = ConValue;
            }
            public String Data
            {
                get { return DataValue.ToString("yyyy-MM-dd HH:mm:ss"); }
                set { DataValue = DateTime.Parse(value); }
            }
            public String Type
            {
                get { return TypeValue; }
                set { TypeValue = value; }
            }
            public String Con
            {
                get { return ConValue; }
                set { ConValue = value; }
            }


        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
