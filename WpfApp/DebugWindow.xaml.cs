﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Dynamic;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;

    namespace WpfApp
{
    public delegate void InvUpdate(String a);

    /// <summary>
    /// Logika interakcji dla klasy DebugWindow.xaml
    /// </summary>
    public partial class DebugWindow : Window
    {

        List<ListBoxItem> Logs;
        UserConsole UC;
        int ListLength;
        private bool IsVisible;
        public System.IO.Stream Cons;

        public DataContextModel DC;

        public static DebugWindow ins;

        public static Thread th;

        public static Object Data;

        public Action<string> updateAction;

        public Func<string> updateActionStr;

        public static void Inv(String a) {
            //Application.GetContentStream(Application.Current.Resources.Values.);
            //Console.WriteLine(App.Current.Windows.ToString());
            DateTime dt = DateTime.Now;
            String type = "Console";
            DebugWindow.ins.Upd(a, type,dt);

        }

        

        public void Upd(String a,String t,DateTime dt) {
            this.AddConsoleLog(a, t,dt);
            UC.saveJson();
        } 


        public DebugWindow()
        {
            
            this.updateAction = new Action<string>((string zz) => { this.PPP.Text = zz; });

            Thread.BeginThreadAffinity();
            DebugWindow.th = Thread.CurrentThread;
            this.DC = new DataContextModel();
            this.DataContext = this.DC;
            DebugWindow.ins = this;
            // Util.LogString(this.DC.consoleText);
            //App.Log(this);
            UC = new UserConsole();
            InitializeComponent();
            DebugWindow.Data= this.PPP.Text.Clone();
            //this.Dispatcher.BeginInvoke(this.updateAction, System.Windows.Threading.DispatcherPriority.Normal, this.PPP.Text);

            //, ref App.consol.Input);
            //this.PPP.SetBinding(a, bb);
            //this.refTo(ref App.consol.Input,ref this.PPP.Content);

            //this.PPP.
            this.Top = 0;
            this.Height = this.GetScreenHeight() - 20;
            this.Width = 300;
            this.Left = this.GetScreenWidth() - 300;
            this.ListLength = 0;
            this.IsVisible = false;

        }

        public void Deb() {
            /*      this.AddConsoleLog("asdasd","Log");
                         this.AddConsoleLog("asdd11","Log");
                         this.AddConsoleLog("asdd11","Log");
                         this.AddConsoleLog("asdd11","Log");
                         this.AddConsoleLog("asdd16","Log");
                         this.AddConsoleLog("asdd16","Log");
                         this.AddConsoleLog("asdd16","Log");
                         this.AddConsoleLog("asdd16","Log");
                         this.AddConsoleLog("asdd20","Log");
                         this.AddConsoleLog("asdd27","Log");
                         this.AddConsoleLog("asdd27","Log");
                         this.AddConsoleLog("asdd27","Log");
                         this.AddConsoleLog("asdd27","Log");
                         this.AddConsoleLog("asdd27","Log");
                         this.AddConsoleLog("asdd27","Log");
              UC.saveJson(); 
                foreach (var it in UC.logs) {
                  Console.WriteLine(it.Content);
              } */
        }
        public View.Log AddConsoleLog(String con, String type,DateTime dt) {
            UC.AddConsoleLog(con, type,dt);
            return this.AddConsoleLogItem(con, type,dt);
        }

        public View.Log AddConsoleLogItem(String con, String Type,DateTime dt = new DateTime()) {
            View.Log ConLogItem = new View.Log(con,Type,dt);
            ConsoleLogs.RowDefinitions.Add(ConLogItem.getRowDefinition());
            Grid.SetRow(ConLogItem, this.ListLength);
            ConsoleLogs.Children.Add(ConLogItem);
            ConsoleLogs.ApplyTemplate();
            this.ListLength++;
            return ConLogItem;
        }
        public void ToogleVisiblity() {
            if (this.IsVisible)
            {
                this.Hide();
                this.IsVisible = false;
            }
            else
            {

                this.Show();
                this.IsVisible = true;
            }
        }
        public void LoadLogs()
        {
            if (UC.logs.Count()>0)
            {
                for (int i = 0; i < ((int)UC.logs.Count()); i++)
                {
                    Console.WriteLine("Content");
                    Console.WriteLine(UC.logs[i].ToString());
                    DebugLog dl = (DebugLog)UC.logs[i];
                    this.AddConsoleLogItem(dl.Content, dl.Type,dl.OutTime);
                }
            }
        }
        public Double GetScreenWidth()
        {
            return System.Windows.SystemParameters.PrimaryScreenWidth;
        }
        public Double GetScreenHeight() {
            return System.Windows.SystemParameters.PrimaryScreenHeight;
        }

        public class DataContextModel:Object {
            public string consoleText;
            public DataContextModel() {
                this.consoleText = "";
                    }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int d = 0;
           
            //ConsoleExtend.AllocConsole();
            //Stream sss = Console.OpenStandardInput();
            
            Console.WriteLine(d.ToString()+" /-----------------------------------DEBUG-----------------------------------/");

            Console.Write(App.consol.Input);

            d++;
            Console.WriteLine(d.ToString() + " /-----------------------------------DEBUG-----------------------------------/");

           // App.consol.Runer();
            //  this.Str.Flush();
            //Console.Write((String)Newtonsoft.Json.JsonConvert.SerializeObject(App.consol, Newtonsoft.Json.Formatting.Indented));

        }
        
    
    }
    
   

}
